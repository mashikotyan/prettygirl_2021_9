#!c:\users\etuko\programing\prettygirl_2021_9\prettyenv\scripts\python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'ginza==5.0.2','console_scripts','ginza'
__requires__ = 'ginza==5.0.2'
import re
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
    sys.exit(
        load_entry_point('ginza==5.0.2', 'console_scripts', 'ginza')()
    )
